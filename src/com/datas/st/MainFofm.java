package com.datas.st;

import java.awt.AWTException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MainFofm extends javax.swing.JFrame {
    
    TestCases myTestCases = new TestCases();
    String instanceName;
    String uName;
    String uPass;
    String ip;
    Integer testCase;

    /**
     * Creates new form ContactEditor
     */
    public MainFofm() {
        initComponents();
    }

    
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jComboBox1Scen = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jTextField1uName = new javax.swing.JTextField();
        jPasswordField2uPass = new javax.swing.JPasswordField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jComboBox3InstName = new javax.swing.JComboBox();
        jButton1StartTest = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1Log = new javax.swing.JTextArea();
        jButton5 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("AutoSiebel");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Тесты"));

        jComboBox1Scen.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "0. Вход", "1. Контакт (КДТ) - Файл", "2. Контакт (КДТА)  - Файл - неактивно", "3. Контакт (КДТ) - Сгенерировать", "4. Контакт (КДТА) - Сгенерировать- неактивно", "5. Заявка", "6. Активация телефона", "7. Создание заявки", "8. Contact ID" }));
        jComboBox1Scen.setToolTipText("Выберите сценарий тестирования функциональности. Выбирайте, который Вам больше нравится.");
        jComboBox1Scen.setEnabled(false);
        jComboBox1Scen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ScenActionPerformed(evt);
            }
        });

        jLabel1.setText("Выбор сценария");

        jTextField1uName.setBackground(new java.awt.Color(255, 255, 204));
        jTextField1uName.setText("aarbuzov");
        jTextField1uName.setToolTipText("Имя пользователя. Обязательное поле");

        jPasswordField2uPass.setBackground(new java.awt.Color(255, 255, 204));
        jPasswordField2uPass.setText("aarbuzov");
        jPasswordField2uPass.setToolTipText("Пароль. Обязательное поле");

        jLabel2.setText("Логин");

        jLabel3.setText("Пароль");

        jLabel4.setText("Система");

        jComboBox3InstName.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "--------------", "1. DEV (155)", "2. TEST (96)" }));
        jComboBox3InstName.setToolTipText("TEST или DEV");
        jComboBox3InstName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3InstNameActionPerformed(evt);
            }
        });

        jButton1StartTest.setText("Старт");
        jButton1StartTest.setEnabled(false);
        jButton1StartTest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1StartTestActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabel1)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jComboBox1Scen, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .add(jPanel1Layout.createSequentialGroup()
                .add(jLabel2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jTextField1uName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 83, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPasswordField2uPass, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel4)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jComboBox3InstName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                .add(0, 0, Short.MAX_VALUE)
                .add(jButton1StartTest))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField1uName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPasswordField2uPass, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel2)
                    .add(jLabel3)
                    .add(jLabel4)
                    .add(jComboBox3InstName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(3, 3, 3)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBox1Scen, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel1))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 8, Short.MAX_VALUE)
                .add(jButton1StartTest))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("Лог"));

        jTextArea1Log.setColumns(20);
        jTextArea1Log.setLineWrap(true);
        jTextArea1Log.setRows(5);
        jTextArea1Log.setWrapStyleWord(true);
        jScrollPane1.setViewportView(jTextArea1Log);

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jScrollPane1)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 160, Short.MAX_VALUE)
                .addContainerGap())
        );

        jButton5.setText("Выход");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                        .add(0, 0, Short.MAX_VALUE)
                        .add(jButton5))
                    .add(jPanel1, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jButton5)
                .addContainerGap())
        );

        jPanel2.getAccessibleContext().setAccessibleName(" E-mail");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:

        myTestCases.closeDriver();
        System.exit(0);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jComboBox1ScenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ScenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ScenActionPerformed

    private void jComboBox3InstNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3InstNameActionPerformed
        // TODO add your handling code here:
        instanceName = (String) jComboBox3InstName.getSelectedItem();
        jComboBox1Scen.setEnabled(true);
        jButton1StartTest.setEnabled(true);
    }//GEN-LAST:event_jComboBox3InstNameActionPerformed

    private void jButton1StartTestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1StartTestActionPerformed
        // TODO add your handling code here:
        //myTestCases.closeDriver();

        uName = jTextField1uName.getText();
        uPass = jPasswordField2uPass.getText();
        testCase = jComboBox1Scen.getSelectedIndex();
        jTextArea1Log.setText("");
        
        
        if (uName.isEmpty() || uPass.isEmpty()) {
            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + "Не заданы Имя или Пароль");
        } else {
            if (null != testCase) {
                jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" +myTestCases.mySupportFactory.getTime(2)+ " : Старт сценария " + testCase);
                jTextArea1Log.update(jTextArea1Log.getGraphics());
                switch (testCase) {
                    case 0:
                        try {
                            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + "Старт теста : " + jComboBox1Scen.getSelectedItem());
                            switch (jComboBox3InstName.getSelectedIndex()) {
                                case 1:
                                    ip = "10.1.15.155";
                                    break;
                                case 2:
                                    ip = "10.1.16.98";
                                    break;
                            }
                            
                            System.out.println("" + ip);
                            myTestCases.caseLogin(uName, uPass, ip);
                        } catch (Exception e) {
                            System.err.println("Ошибка входа: " + e.getMessage());
                            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + "Ошибка входа: " + e.getMessage() + "\r\n");
                        }
                        break;
                    case 1: //1. Контакт (КДТ) - Файл    
                        jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + "Старт теста : " + jComboBox1Scen.getSelectedItem());
                         {
                            try {
                                myTestCases.caseContactShort(ip);
                            } catch (InterruptedException | IOException ex) {
                                Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                        break;
                    case 3: {
                        try {
                            myTestCases.caseContactShortR(ip);
                        } catch (InterruptedException | IOException ex) {
                            Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    break;
                    case 5: {
                        jTextArea1Log.update(jTextArea1Log.getGraphics());
                        try {
                            myTestCases.checkOppty(ip);
                        } catch (InterruptedException | IOException | AWTException ex) {
                            Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + ex.getLocalizedMessage());
                        }
                    }
                    break;
                    case 6: {
                        jTextArea1Log.update(jTextArea1Log.getGraphics());
                        try {
                            myTestCases.caseValPhone(ip);
                        } catch (InterruptedException | IOException ex) {
                            Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + ex.getLocalizedMessage());
                        } catch (AWTException ex) {
                        Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    }
                    break;
                    case 7: {
                        jTextArea1Log.update(jTextArea1Log.getGraphics());
                        try {
                            myTestCases.caseCreateOppty(ip);
                        } catch (InterruptedException | IOException ex) {
                            Logger.getLogger(MainFofm.class.getName()).log(Level.SEVERE, null, ex);
                            jTextArea1Log.setText(jTextArea1Log.getText() + "\r\n" + ex.getLocalizedMessage());
                        }
                    }
                    break;
                    case 8:  {

                    }
                    break;
                    default:
                        break;
                }
            }
        }
    }//GEN-LAST:event_jButton1StartTestActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            javax.swing.UIManager.LookAndFeelInfo[] installedLookAndFeels = javax.swing.UIManager.getInstalledLookAndFeels();
            for (int idx = 0; idx < installedLookAndFeels.length; idx++) {
                if ("Nimbus".equals(installedLookAndFeels[idx].getName())) {
                    javax.swing.UIManager.setLookAndFeel(installedLookAndFeels[idx].getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFofm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFofm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFofm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFofm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new MainFofm().setVisible(true);
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton1StartTest;
    private javax.swing.JButton jButton5;
    private javax.swing.JComboBox jComboBox1Scen;
    private javax.swing.JComboBox jComboBox3InstName;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPasswordField jPasswordField2uPass;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextArea jTextArea1Log;
    private javax.swing.JTextField jTextField1uName;
    // End of variables declaration//GEN-END:variables
}
