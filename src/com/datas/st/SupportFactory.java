/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datas.st;

import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import javax.imageio.ImageIO;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author amigo
 */
public class SupportFactory {

    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
    DateFormat dateFormatLog = new SimpleDateFormat("HH:mm:ss:SSS");
    WebDriver driver = null;
    WebDriverWait wait = null;

    public void setDriver(String browser, String driver) {
        if (System.getProperty("os.name").contains("Windows")) {
            System.setProperty("webdriver." + browser + ".driver", "C:\\Users\\egar_oarbuzov\\Documents\\NetBeansProjects\\AutoSiebel\\libs\\" + driver + ".exe");
        } else {
            System.setProperty("webdriver." + browser + ".driver", "/libs/" + driver + "");
        }
    }

    //Логин в сибель
    public WebDriver login(String l, String p, String ip) throws AWTException {

        try {

            // Create a new instance of the Firefox driver
            // Notice that the remainder of the code relies on the interface, 
            // not the implementation.
            setDriver("chrome", "chromedriver");

            driver = new ChromeDriver();
            wait = new WebDriverWait(driver, 15);

            driver.manage().window().maximize();

            driver.get("https://" + ip + "/fins_rus_ui/start.swe");

            WebElement weSWEUserName = driver.findElement(By.name("SWEUserName"));
            WebElement weSWEPassword = driver.findElement(By.name("SWEPassword"));
            weSWEUserName.sendKeys(l);
            weSWEPassword.sendKeys(p);
            takeScreenSL(driver);
            driver.findElement(By.id("s_swepi_22")).sendKeys(Keys.RETURN);

        } catch (IOException e) {
            driver.quit();
            System.err.println("Ошибка входа в Siebel: " + e.getMessage());
        }

        return driver;
    }

    //Создание контакта
    public void createContactShort(WebDriver driver, String lName, String fName, String mName, String gender, String residency, String inn, String bDate, String bPlace) throws InterruptedException, AWTException, IOException {

        //Создание
        driver.findElement(By.id("s_1_1_70_0_Ctrl")).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //Фамилия
        driver.findElement(By.name("s_1_1_17_0")).sendKeys(lName);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Имя
        driver.findElement(By.name("s_1_1_19_0")).sendKeys(fName);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Отчество
        driver.findElement(By.name("s_1_1_21_0")).sendKeys(mName);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Пол
        driver.findElement(By.name("s_1_1_34_0")).sendKeys(gender);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Рез-ность
        driver.findElement(By.name("s_1_1_25_0")).sendKeys(residency);
        driver.findElement(By.name("s_1_1_25_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Инн
        driver.findElement(By.name("s_1_1_27_0")).sendKeys(inn);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Дата рождения
        driver.findElement(By.name("s_1_1_30_0")).sendKeys(bDate);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Гражданство
        //driver.findElement(By.name("s_1_1_23_0")).sendKeys("Україна");
        //driver.findElement(By.name("s_1_1_23_0")).sendKeys(Keys.TAB);
        //место рождения
        driver.findElement(By.name("s_1_1_32_0")).sendKeys(bPlace);
        Thread.sleep(1000);
        takeScreenSL(driver);

        /*
        //Сохранение
        driver.findElement(By.name("s_1_1_17_0")).sendKeys(Keys.chord(Keys.CONTROL, "s"));
        Thread.sleep(500);
        takeScreen();
        * */
    }

    //фоточка
    public void takeScreen() throws AWTException, IOException {
        BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
        ImageIO.write(image, "png", new File("log/" + getTime(1) + ".png"));
    }

    //фоточка от селениума
    public void takeScreenSL(WebDriver driver) throws AWTException, IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(scrFile, new File("log/" + getTime(1) + ".png"));
    }

    //Создание нового документа
    public void createNewDoc(WebDriver driver, String docType, String docSeria, String docNumber, String docIssueDate, String docIssuer, String docValidDate) throws InterruptedException, AWTException, IOException {

        //Документ - иконка
        driver.findElement(By.id("s_1_1_8_0_icon")).click();
        Thread.sleep(2000);
        takeScreen();

        //Документ - создать
        driver.findElement(By.name("s_3_1_81_0")).click();
        Thread.sleep(1000);
        takeScreen();

        //Тип документа
        driver.findElement(By.id("1_s_3_l_Doc_Type")).click();//1_s_3_l_Doc_Type
        driver.findElement(By.name("Doc_Type")).sendKeys(docType);
        takeScreen();

        //Seria
        driver.findElement(By.id("1_s_3_l_Seria")).click();
        driver.findElement(By.name("Seria")).sendKeys(docSeria);
        takeScreen();

        //Number
        driver.findElement(By.id("1_s_3_l_Number")).click();
        driver.findElement(By.name("Number")).sendKeys(docNumber);
        takeScreen();

        //Issued_Date
        driver.findElement(By.id("1_s_3_l_Issued_Date")).click();
        driver.findElement(By.name("Issued_Date")).sendKeys(docIssueDate);
        takeScreen();

        //Issued_By
        driver.findElement(By.id("1_s_3_l_Issued_By")).click();
        driver.findElement(By.name("Issued_By")).sendKeys(docIssuer);
        takeScreenSL(driver);

        //Valid_To_Date
        driver.findElement(By.id("1_s_3_l_Valid_To_Date")).click();
        driver.findElement(By.name("Valid_To_Date")).sendKeys(docValidDate);
        driver.findElement(By.name("Valid_To_Date")).sendKeys(Keys.TAB);
        takeScreenSL(driver);

        //Photo25_Flg
        driver.findElement(By.id("1_Photo25_Flg")).click();
        driver.findElement(By.id("1_Photo25_Flg")).sendKeys(Keys.TAB);
        takeScreenSL(driver);

        //Photo45_Flg
        driver.findElement(By.id("1_Photo45_Flg")).click();
        driver.findElement(By.id("1_Photo45_Flg")).sendKeys(Keys.TAB);
        takeScreenSL(driver);

        //SSA_Primary_Field
        driver.findElement(By.id("1_SSA_Primary_Field")).click();
        takeScreenSL(driver);
        Thread.sleep(1000);

        /*
        //Record_Num // ID card only
        driver.findElement(By.id("1_s_3_l_Record_Num")).click();
        driver.findElement(By.name("Record_Num")).sendKeys("12345-12345678");
        supportFactory.takeScreenSL(driver);
         */
        //OK
        driver.findElement(By.id("s_3_1_82_0_Ctrl")).click();
        Thread.sleep(1000);
        takeScreen();
        Thread.sleep(1000);

    }

    //Создание нового телефона
    public void createNewPhone(WebDriver driver, String newButtonName, String okButtonName, String phoneCode, String phoneNumber) throws IOException, AWTException, InterruptedException {
        //МВГ Телефона
        driver.findElement(By.id("s_1_1_1_0_icon")).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //МВГ Телефона -Создать //s_4_1_70_0_Ctrl
        //driver.findElement(By.id("s_3_1_70_0_Ctrl")).click();
        driver.findElement(By.name(newButtonName)).click(); //s_4_1_70_0
        Thread.sleep(1000);
        takeScreenSL(driver);

        //МВГ Телефона - код
        //driver.findElement(By.id("1_s_8_l_EGR_Code")).sendKeys("50");
        driver.findElement(By.id("1_EGR_Country")).sendKeys(Keys.TAB);
        Thread.sleep(100);
        takeScreenSL(driver);

        //МВГ Телефона - код
        //driver.findElement(By.id("1_s_8_l_EGR_Code")).sendKeys("50");
        driver.findElement(By.name("EGR_Code")).sendKeys(phoneCode);
        driver.findElement(By.name("EGR_Code")).sendKeys(Keys.TAB);
        takeScreenSL(driver);

        //МВГ Телефона - номер
        //driver.findElement(By.id("1_s_8_l_EGR_Code")).sendKeys("50");
        driver.findElement(By.name("Phone_Number")).sendKeys(phoneNumber);
        driver.findElement(By.name("Phone_Number")).sendKeys(Keys.TAB);
        takeScreenSL(driver);

        //МВГ Телефона -OK
        //driver.findElement(By.name("s_3_1_81_0")).click();
        driver.findElement(By.name(okButtonName)).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //Сохранение
        driver.findElement(By.name("s_1_1_17_0")).sendKeys(Keys.chord(Keys.CONTROL, "s"));
        Thread.sleep(500);
        takeScreen();
    }

    //Создание адреса
    public void createNewAddress(WebDriver driver) throws InterruptedException, AWTException, IOException {

        //МВГ Адреса
        driver.findElement(By.id("s_1_1_46_0_icon")).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //Создание адреса
        driver.findElement(By.id("s_3_1_70_0_Ctrl")).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //Тип регистрации
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.chord(Keys.SHIFT, Keys.HOME));
        Thread.sleep(100);
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.DELETE);
        driver.findElement(By.name("s_3_2_89_0")).sendKeys("Перебування (тимчасового переб");
        driver.findElement(By.name("s_3_2_89_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Индекс
        driver.findElement(By.name("s_3_2_91_0")).sendKeys("12345");
        Thread.sleep(100);
        takeScreenSL(driver);

        //Область
        driver.findElement(By.name("s_3_2_92_0")).sendKeys("Вiнницька область");
        driver.findElement(By.name("s_3_2_92_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //Район
        driver.findElement(By.name("s_3_2_93_0")).sendKeys("Вiнницький");
        Thread.sleep(100);
        takeScreenSL(driver);

        //тип НП
        driver.findElement(By.name("s_3_2_94_0")).sendKeys("мiсто");
        driver.findElement(By.name("s_3_2_94_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //НП
        driver.findElement(By.name("s_3_2_95_0")).sendKeys("Вiнницяя");
        Thread.sleep(100);
        takeScreenSL(driver);

        //тип улицы
        driver.findElement(By.name("s_3_2_96_0")).sendKeys("вулиця");
        driver.findElement(By.name("s_3_2_96_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //улица
        driver.findElement(By.name("s_3_2_97_0")).sendKeys("Вiнницькаяяяяя");
        Thread.sleep(100);
        takeScreenSL(driver);

        //тип дома
        driver.findElement(By.name("s_3_2_98_0")).sendKeys("будинок");
        driver.findElement(By.name("s_3_2_98_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //дом
        driver.findElement(By.name("s_3_2_99_0")).sendKeys("100");
        Thread.sleep(100);
        takeScreenSL(driver);

        //корпус
        driver.findElement(By.name("s_3_2_100_0")).sendKeys("1");
        Thread.sleep(100);
        takeScreenSL(driver);

        //тип помещения
        driver.findElement(By.name("s_3_2_101_0")).sendKeys("кімната");
        driver.findElement(By.name("s_3_2_101_0")).sendKeys(Keys.RETURN);
        Thread.sleep(100);
        takeScreenSL(driver);

        //номер помещения
        driver.findElement(By.name("s_3_2_102_0")).sendKeys("1");
        Thread.sleep(100);
        takeScreenSL(driver);

        //срок проживания
        driver.findElement(By.name("s_3_2_103_0")).sendKeys("200");
        Thread.sleep(100);
        takeScreenSL(driver);

        //Совпадает
        driver.findElement(By.name("s_3_2_104_0")).click();
        Thread.sleep(1000);
        takeScreenSL(driver);

        //Сохранить
        driver.findElement(By.id("s_3_1_75_0_Ctrl")).click();
        Thread.sleep(1000);
        takeScreen();

        //Закрыть
        driver.findElement(By.id("s_3_1_81_0_Ctrl")).click();
        //supportFactory.takeScreenSL(driver);
        takeScreen();
        Thread.sleep(1000);
        //supportFactory.takeScreenSL(driver);
        takeScreen();

    }

    //Закрытие теста
    public void closeDriver(WebDriver driver) {

        if (driver != null) {
            driver.quit();
        }

    }

    public void gotoView(String url) throws InterruptedException {
        driver.get(url);
        Thread.sleep(5000);

    }

    public void gotoOppty(WebDriver driver, String opptyId) throws IOException, AWTException, InterruptedException {
        WebElement pSource = null;
        //Запрос
        driver.findElement(By.id("s_1_1_19_0_Ctrl")).click();
        Thread.sleep(500);
        takeScreen();

        //Ввод и поиск
        driver.findElement(By.id("1_s_1_l_Opportunity__")).click();
        driver.findElement(By.id("1_Opportunity__")).sendKeys(opptyId);
        driver.findElement(By.id("1_Opportunity__")).sendKeys(Keys.ENTER);
        driver.findElement(By.id("1_Opportunity__")).sendKeys(Keys.TAB);
        Thread.sleep(500);
        takeScreen();
        driver.findElement(By.id("1_s_1_l_Opportunity__")).click();

        Thread.sleep(1000);
        takeScreen();
    }

    public void gotoForm(WebDriver driver, String formname) throws InterruptedException, AWTException, IOException {

        for (int i = 1; i > 0; i++) {

            try {
                Thread.sleep(1000);
                driver.findElement(By.linkText(formname)).click();
                System.out.println(getTime(2) + ": вход на " + formname);

                break;
            } catch (InterruptedException e) {
                System.err.println("Ошибка перехода на полную форму: " + e.getLocalizedMessage());
            }

        }


        /*
        try {
            //1_s_9_l_Source
            pSource = driver.findElement(By.id("1_s_9_l_Source"));

        } catch (Exception e) {
            System.err.println("" + e.getLocalizedMessage());
        }

        if (pSource != null) {
            System.out.println("getText: " + pSource.getText());
        } else {
            System.out.println("Источники доходов не обнаружены");
            //Доходы - создать
            driver.findElement(By.id("s_9_1_8_0_Ctrl")).click();
            //driver.findElement(By.name("s_9_1_8_0")).click();
            //driver.findElement(By.className("ui-jqgrid-bdiv")).sendKeys(Keys.chord(Keys.CONTROL, "n"));
            Thread.sleep(1000);
            driver.findElement(By.id("1_Source")).sendKeys("Пенсія за віком ");
            driver.findElement(By.id("1_Source")).sendKeys(Keys.TAB);
            Thread.sleep(500);
            takeScreenSL(driver);

            //Сумма
            driver.findElement(By.id("1_EGR_Avg_Proved_Amount")).sendKeys("1000");
            driver.findElement(By.id("1_EGR_Avg_Proved_Amount")).sendKeys(Keys.TAB);
            Thread.sleep(500);
            takeScreenSL(driver);
        }
         */
    }

    public Integer getRandomNum(int min, int max) {
        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }

    public String gNames() {

        Connection c = null;
        Statement stmt = null;
        String fullName = null;
        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:C:\\Install\\AutoSiebel\\db\\autosiebel.db");
            c.setAutoCommit(false);
            String fname = null;
            String gender = null;
            String mname = null;
            String lname = null;
            String cityName = null;
            String suffix;
            String streetName = null;
            ResultSet rs;

            stmt = c.createStatement();
            //Имя
            rs = stmt.executeQuery("SELECT * FROM names where rnum = " + getRandomNum(1, 1109) + ";");
            while (rs.next()) {
                fname = rs.getString("name");
                gender = rs.getString("gender");
            }

            //Окончание отчества
            if ("Ж".equals(gender)) {
                suffix = "івна";
                gender = "Жіноча";
            } else {
                suffix = "ович";
                gender = "Чоловіча";
            }
            //Отчество
            rs = stmt.executeQuery("SELECT * FROM names where rnum = " + getRandomNum(1, 525) + ";");
            while (rs.next()) {
                mname = rs.getString("name") + suffix;
            }

            //Фамилия
            rs = stmt.executeQuery("SELECT last_name FROM lname where rnum = " + getRandomNum(1, 174) + ";");
            while (rs.next()) {
                lname = rs.getString("last_name");
            }
            rs.close();
            stmt.close();

            //Город
            rs = stmt.executeQuery("SELECT * FROM names where rnum = " + getRandomNum(1, 525) + ";");
            while (rs.next()) {
                cityName = rs.getString("name");
            }

            //Улица
            rs = stmt.executeQuery("SELECT * FROM names where rnum = " + getRandomNum(1, 525) + ";");
            while (rs.next()) {
                streetName = rs.getString("name") + "ська";
            }

            //         0                           1                           2                           3              4                              5
            fullName = lname.toUpperCase() + ":" + fname.toUpperCase() + ":" + mname.toUpperCase() + ":" + gender + ":" + cityName.toUpperCase() + ":" + streetName;

            c.close();
        } catch (ClassNotFoundException | SQLException e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        return fullName;
    }

    public void searchContact(WebDriver driver) throws InterruptedException {
        driver.findElement(By.id("1_s_2_l_EGR_Siebel_Id")).click();
        driver.findElement(By.id("1_s_2_l_EGR_Siebel_Id")).sendKeys(Keys.chord(Keys.ALT, "q"));
        Thread.sleep(500);
        driver.findElement(By.id("1_EGR_Siebel_Id")).sendKeys("1-87X8QJ"); //EGR_Siebel_Id
        driver.findElement(By.id("1_EGR_Siebel_Id")).sendKeys(Keys.ENTER);
        Thread.sleep(500);
        System.out.println("Phone validation state : " + driver.findElement(By.xpath("//input[@aria-labelledby='EGRPhoneVerify_Label']")).getAttribute("value"));

    }

    public String checkPhoneValState(WebDriver driver) {
        String state = driver.findElement(By.xpath("//input[@aria-labelledby='EGRPhoneVerify_Label']")).getAttribute("value");
        return state;
    }

    public void verPhone(WebDriver driver) throws InterruptedException {

        //Дождаться кнопки Верификация Телефона
        WebElement verPhoneButton = (new WebDriverWait(driver, 10))
                .until((ExpectedCondition<WebElement>) (WebDriver d) -> d.findElement(By.xpath("//button[@data-display='Верификация Телефона']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-display='Верификация Телефона']")));
        verPhoneButton.click();

        
        
        //Дождаться кнопку Отправить код
        WebElement verCodeButton = (new WebDriverWait(driver, 10))
                .until((ExpectedCondition<WebElement>) (WebDriver d) -> d.findElement(By.xpath("//button[@data-display='Отправить код']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-display='Отправить код']")));
        verCodeButton.click();
        Thread.sleep(500);

        for (int i = 1; i > 0; i++) {
            try {
                Thread.sleep(500);
                driver.switchTo().alert().accept();
                break;
            } catch (InterruptedException e) {
                System.err.println("ошибка алерта : " + i + " " +  e.getLocalizedMessage());
            }
        }
        
        driver.findElement(By.xpath("//input[@aria-labelledby='EGRCellularPhoneVerCode_Label']")).sendKeys("69"); //
        Thread.sleep(1500);
                //Дождаться кнопку Отправить код
        WebElement checkCodeButton = (new WebDriverWait(driver, 10))
                .until((ExpectedCondition<WebElement>) (WebDriver d) -> d.findElement(By.xpath("//button[@data-display='Проверить код']")));
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[@data-display='Проверить код']")));
        checkCodeButton.click();
        //driver.findElement(By.xpath("//button[@data-display='Проверить код']")).click(); //
        Thread.sleep(500);

        for (int i = 1; i > 0; i++) {
            try {
                Thread.sleep(500);
                driver.switchTo().alert().accept();
                break;
            } catch (InterruptedException e) {
                System.err.println("ошибка алерта : " + i + " " +  e.getLocalizedMessage());
            }
        }
    }

    public void createNewOppty(WebDriver driver) throws InterruptedException {

        driver.findElement(By.xpath("//a[@name='Last Name']")).click(); //
        Thread.sleep(5000);
        driver.findElement(By.name("s_1_1_19_0")).click();
        Thread.sleep(3000);
        driver.findElement(By.xpath("//input[@aria-labelledby='1_s_1_l_Opportunity__ s_1_l_EGR_Product_Type s_1_l_altCombo']")).sendKeys("Споживчий кредит"); //
        driver.findElement(By.xpath("//input[@aria-labelledby='1_s_1_l_Opportunity__ s_1_l_EGR_Product_Type s_1_l_altCombo']")).sendKeys(Keys.ENTER); //
        driver.findElement(By.xpath("//input[@aria-labelledby='1_s_1_l_Opportunity__ s_1_l_EGR_Product_Type s_1_l_altCombo']")).sendKeys(Keys.TAB); //
        Thread.sleep(500);
        driver.findElement(By.xpath("//input[@name='EGR_Program']")).sendKeys("Еко оселя"); //
        driver.findElement(By.xpath("//input[@name='EGR_Program']")).sendKeys(Keys.TAB); //
        Thread.sleep(5000);
        driver.findElement(By.xpath("//td[@aria-labelledby='s_1_l_Opportunity__ s_1_l_altLink']")).click(); //

    }

    public String getTime(Integer fType) {
        Date date = new Date();
        String rtnDt = null;
        switch (fType) {
            case 1:
                rtnDt = dateFormat.format(date);
                break;
            case 2:
                rtnDt = dateFormatLog.format(date);
                break;
            default:
                break;
        }
        return rtnDt;
    }
    
    public void saveContId(WebDriver driver){
    String contactId = driver.findElement(By.xpath("//td[@id='1_s_2_l_EGR_Siebel_Id']")).getText(); //    
        System.out.println("" + contactId);
    }   
            

    public String generateDocSeria() {
        char[] chars = "ЦУКЕНГШЩЗХФВАПРОЛШДЖЯЧСМИТБЮ".toCharArray();
        String seria = "";
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < 2; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        seria = sb.toString();
        return seria;
    }

}
