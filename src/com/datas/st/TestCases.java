/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.datas.st;

import com.opencsv.CSVReader;
import java.awt.AWTException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.WebDriver;

/**
 *
 * @author amigo
 */
public class TestCases {

    SupportFactory mySupportFactory = new SupportFactory();
    WebDriver myDriver = null;

    public void caseLogin(String l, String p, String ip) {
        try {
            myDriver = mySupportFactory.login(l, p, ip);
        } catch (AWTException ex) {
            Logger.getLogger(TestCases.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void caseContactShort(String ip) throws InterruptedException, IOException {
        try {
            //Переходим 1 раз
            mySupportFactory.gotoView("https://" + ip + "/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Main+View+-+My&SWERF=1&SWEHo=" + ip + "&SWEBU=1");

            String CSV_PATH = "C:\\Install\\AutoSiebel\\cases\\contact.csv";

            CSVReader reader = new CSVReader(new FileReader(CSV_PATH));
            String[] csvCell;
            while ((csvCell = reader.readNext()) != null) {
                String lName = csvCell[0];
                String fName = csvCell[1];
                String mName = csvCell[2];
                String gender = csvCell[3];
                String residency = csvCell[4];
                String inn = csvCell[5];
                String bDate = csvCell[6];
                String bPlace = csvCell[7];
                String docType = csvCell[8];
                String docSeria = csvCell[9];
                String docNumber = csvCell[10];
                String docIssueDate = csvCell[11];
                String docIssuer = csvCell[12];
                String docValidDate = csvCell[13];
                String phoneCode = csvCell[14];
                String phoneNumber = csvCell[15];

                mySupportFactory.createContactShort(myDriver, lName, fName, mName, gender, residency, inn, bDate, bPlace);
                mySupportFactory.createNewDoc(myDriver, docType, docSeria, docNumber, docIssueDate, docIssuer, docValidDate);
                mySupportFactory.createNewPhone(myDriver, "s_4_1_70_0", "s_4_1_81_0", phoneCode, phoneNumber);
            }

        } catch (AWTException ex) {
            Logger.getLogger(TestCases.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void caseContactShortR(String ip) throws InterruptedException, IOException {
        try {
            //Переходим 1 раз
            mySupportFactory.gotoView("https://" + ip + "/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Main+View+-+My&SWERF=1&SWEHo=" + ip + "&SWEBU=1");

            String[] contactData = nGenerator().split(":");

            String CSV_PATH = "C:\\Install\\AutoSiebel\\cases\\contact.csv";
            CSVReader reader = new CSVReader(new FileReader(CSV_PATH));
            String[] csvCell;
            while ((csvCell = reader.readNext()) != null) {
                String lName = csvCell[0];
                String fName = csvCell[1];
                String mName = csvCell[2];
                String gender = csvCell[3];
                String residency = csvCell[4];
                String inn = csvCell[5];
                String bDate = csvCell[6];
                String bPlace = csvCell[7];
                String docType = csvCell[8];
                String docSeria = csvCell[9];
                String docNumber = csvCell[10];
                String docIssueDate = csvCell[11];
                String docIssuer = csvCell[12];
                String docValidDate = csvCell[13];
                String phoneCode = csvCell[14];
                String phoneNumber = csvCell[15];

                mySupportFactory.createContactShort(myDriver, contactData[0], contactData[1], contactData[2], contactData[3], residency, inn, bDate, contactData[4]);
                mySupportFactory.createNewDoc(myDriver, docType, mySupportFactory.generateDocSeria(), mySupportFactory.getRandomNum(111111, 999999).toString(), docIssueDate, docIssuer, docValidDate);
                mySupportFactory.createNewPhone(myDriver, "s_4_1_70_0", "s_4_1_81_0", phoneCode, phoneNumber);
                mySupportFactory.verPhone(myDriver);
                mySupportFactory.saveContId(myDriver);
                mySupportFactory.createNewOppty(myDriver);
            }

        } catch (AWTException ex) {
            Logger.getLogger(TestCases.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void checkOppty(String ip) throws InterruptedException, IOException, AWTException {
        mySupportFactory.gotoView("https://" + ip + "/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+All+Opportunity+Administrator&SWERF=1&SWEHo=" + ip + "&SWEBU=1");
        mySupportFactory.gotoOppty(myDriver, ip);
        mySupportFactory.gotoForm(myDriver, "Полная форма");
        Thread.sleep(5000);
        mySupportFactory.gotoForm(myDriver, "Короткая форма");
    }
    public void caseValPhone(String ip) throws InterruptedException, IOException, AWTException {
        try {
            //Переходим 1 раз
            mySupportFactory.gotoView("https://" + ip + "/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Main+View+-+My&SWERF=1&SWEHo=" + ip + "&SWEBU=1");
            mySupportFactory.searchContact(myDriver);
            if (!"Y".equals(mySupportFactory.checkPhoneValState(myDriver))){
            mySupportFactory.verPhone(myDriver);
            }
        } catch (InterruptedException e) {
            System.err.println("" + e.getLocalizedMessage());
        }
    }

    public void caseCreateOppty(String ip) throws InterruptedException, IOException {

        try {
            //Переходим 1 раз
            mySupportFactory.gotoView("https://" + ip + "/fins_rus_ui/start.swe?SWECmd=GotoView&SWEView=EGR+Contact+Main+View+-+My&SWERF=1&SWEHo=" + ip + "&SWEBU=1");
            mySupportFactory.searchContact(myDriver); //Есть  контакт!!!
            if (!"Y".equals(mySupportFactory.checkPhoneValState(myDriver))){ //Проверка валидации телефона
            mySupportFactory.verPhone(myDriver);
            }
            mySupportFactory.createNewOppty(myDriver); // Переход, на форму создания новых заявок
            
        } catch (InterruptedException e) {
            System.err.println("" + e.getLocalizedMessage());
        }

    }

    public void closeDriver() {
        mySupportFactory.closeDriver(myDriver);
    }

    public String nGenerator() {
        String contactData = mySupportFactory.gNames();
        return contactData;
    }
}
